package com.example.demo.service;

import com.example.demo.entity.Info;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sinovatio
 * @since 2019-10-31
 */
public interface IInfoService extends IService<Info> {

}
