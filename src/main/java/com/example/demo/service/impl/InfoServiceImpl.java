package com.example.demo.service.impl;

import com.example.demo.entity.Info;
import com.example.demo.dao.InfoMapper;
import com.example.demo.service.IInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sinovatio
 * @since 2019-10-31
 */
@Service
public class InfoServiceImpl extends ServiceImpl<InfoMapper, Info> implements IInfoService {

}
