package com.example.demo.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.example.demo.config.PageList;
import com.example.demo.config.Response;
import com.example.demo.config.ResponseUtils;
import com.example.demo.config.SwaggerAutoConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entity.Info;
import com.example.demo.service.IInfoService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sinovatio
 * @since 2019-10-31
 */
@Api(" 服务")
@RestController
public class InfoController {

    @Autowired
    private IInfoService infoApi;

    @ApiOperation("Info表查询")
    @ApiImplicitParam(name = "version", paramType = "path", allowableValues = SwaggerAutoConfig.COMPATIBLE_VERSION, required = true)
    @PostMapping("{version}/pv/infos")
    public Response<Boolean> insert(@RequestBody Info info) {
        return ResponseUtils.returnObjectSuccess(infoApi.insert(info));
    }

    @ApiOperation("编辑单个Info")
    @ApiImplicitParam(name = "version", paramType = "path", allowableValues = SwaggerAutoConfig.COMPATIBLE_VERSION, required = true)
    @PutMapping("{version}/pv/infos/{uid}")
    public Response<Boolean> updateById(@PathVariable("uid") Integer uid, @RequestBody Info info) {
        info.setId(uid);
        return ResponseUtils.returnObjectSuccess(infoApi.updateById(info));
    }

    @ApiOperation("查询单个Info")
    @ApiImplicitParam(name = "version", paramType = "path", allowableValues = SwaggerAutoConfig.COMPATIBLE_VERSION, required = true)
    @GetMapping("{version}/pb/infos/{uid}")
    public Response<Info> selectById(@PathVariable("uid") Long uid) {
        return ResponseUtils.returnObjectSuccess(infoApi.selectById(uid));
    }

    @ApiOperation("查询分页Info")
    @ApiImplicitParam(name = "version", paramType = "path", allowableValues = SwaggerAutoConfig.COMPATIBLE_VERSION, required = true)
    @GetMapping("{version}/pb/infos/action/search")
    public Response<PageList<Info>> selectPage(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                                               @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                                               Info info) {
        Page<Info> page = infoApi.selectPage(new Page<>(pageNo, pageSize), new EntityWrapper<>(info));
        return ResponseUtils.returnObjectSuccess(new PageList<>(page));
    }
}
