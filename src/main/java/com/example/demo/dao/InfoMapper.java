package com.example.demo.dao;

import com.example.demo.entity.Info;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sinovatio
 * @since 2019-10-31
 */
@Repository
public interface InfoMapper extends BaseMapper<Info> {

}
