package com.example.demo.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author sinovatio
 * @since 2019-10-31
 */
@ApiModel("Info实体类")
public class Info implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    @ApiModelProperty(value = "序号")
    private Integer id;
    @TableField("name")
    @ApiModelProperty(value = "姓名")
    private String name;
    @TableField("addr")
    @ApiModelProperty(value = "地址")
    private String addr;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    @Override
    public String toString() {
        return "Info{" +
        "id=" + id +
        ", name=" + name +
        ", addr=" + addr +
        "}";
    }
}
